[Giter8](http://www.foundweekends.org/giter8/) template for spark etl projects that use [delta lake](https://delta.io/).

It incorporates the process of a raw csv file to a delta parquet table. The CSV provides belongs to [TMB Open Data](https://www.tmb.cat/es/sobre-tmb/herramientas-para-desarrolladores/datos-gtfs) the transportation company in Barcelona.
